# StickyScrollView

#### 介绍
粘性ScrollView滚动到顶部指定子View悬浮顶部

Android开发中这样的需求比较多,之前需要写两份布局检测距离后切换展示,比较麻烦且臃肿,使用这个控件只需要把悬浮的控件打个tag即可实现,希望对你有多帮助,有什么好的意见也请多多提出!



#### 样式
图片滑动效果 :
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/092254_d71b9960_7750532.jpeg "Screenshot_2021-07-14-17-52-08-562_cn.com.jmw.stickyscrollview.jpg")

---------

![输入图片说明](https://images.gitee.com/uploads/images/2021/0714/175330_1a2c20ef_7750532.jpeg "Screenshot_2021-07-14-17-52-11-889_cn.com.jmw.stickyscrollview.jpg")



#### 接入流程
- **1.1 在项目build.gradle加入仓库**
```
buildscript {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

- **1.2 在app's build.gradle中添加依赖**

```
dependencies {
  ...
   implementation 'com.gitee.zhaolonglongmayday:sticky-scroll-view:v1.1.1'
}
```

#### 使用说明

```
<cn.com.jmw.mylibrary.StickyScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

  
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">


        <View
            android:layout_width="match_parent"
            android:layout_height="300dp"
            android:background="#B55D52" />


        <View
            android:layout_width="match_parent"
            android:layout_height="300dp"
            android:background="#366DF1" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="100dp"
            android:background="@color/white"
            android:gravity="center"
            android:tag="sticky"
            android:text="我是悬浮标题" />

        <View
            android:layout_width="match_parent"
            android:layout_height="300dp"
            android:background="#A8A8A8" />


        <View
            android:layout_width="match_parent"
            android:layout_height="300dp"
            android:background="#ACACAC" />


        <View
            android:layout_width="match_parent"
            android:layout_height="300dp"
            android:background="#D7BB80" />


        <View
            android:layout_width="match_parent"
            android:layout_height="300dp"
            android:background="#ff3737" />


        <View
            android:layout_width="match_parent"
            android:layout_height="300dp"
            android:background="#E44B46" />


    </LinearLayout>


</cn.com.jmw.mylibrary.StickyScrollView>
```
### 划重点 划重点 划重点!!!
 _**需要悬浮的控件添加的tag
 android:tag="sticky**_  


